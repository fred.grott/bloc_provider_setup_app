library my_bdd_runner;

import 'package:dherkin2/dherkin.dart';
//import 'my_step_defs.dart'; // import stepdefs, mandatory since no auto-scanning happens

dynamic main(dynamic args) {
  run(args).whenComplete(() => print('ALL DONE'));
}